<?php

use Faker\Generator as Faker;

$factory->define(App\Tour::class, function (Faker $faker) {
    return [
        'name'=>$faker->realText(30),
        'itinerary'=>$faker->realText(250),
        'status'=>1
    ];
});

$factory->define(App\Tour_date::class, function (Faker $faker) {
    return [
        'tour_id'=>2,
        'date'=>$faker->date,
        'status'=>1
    ];
});