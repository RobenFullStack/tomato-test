<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourDateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_dates', function (Blueprint $table) {
            //primary key
            $table->increments('id');
            
            $table->date('date');
            $table->tinyInteger('status');
            $table->timestamps();

            //foreign key column
            $table->unsignedInteger('tour_id');

            //add reference
            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_dates',function(Blueprint $table){
            $table->dropForeign('tour_dates_tour_id_foreign');
        });
        
        Schema::dropIfExists('tour_dates');
    }
}
