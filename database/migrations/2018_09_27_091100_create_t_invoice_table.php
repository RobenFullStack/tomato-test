<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_invoices', function (Blueprint $table) {
            //primary key
            $table->increments('id');

            $table->double('amount');
            $table->tinyInteger('status');
            $table->timestamps();

            //foreign key
            $table->unsignedInteger('booking_id');
            
            //add reference
            $table->foreign('booking_id')->references('id')->on('t_bookings')->onDelete('cascade');           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_invoices',function(Blueprint $table){
            $table->dropForeign('t_invoices_booking_id_foreign');
        });
        Schema::dropIfExists('t_invoices');
    }
}
