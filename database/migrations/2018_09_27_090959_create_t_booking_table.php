<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            //primary key
            $table->increments('id');

            $table->date('tour_date');
            $table->tinyInteger('status');
            $table->timestamps();

            //foreign key
            $table->unsignedInteger('tour_id');
            
            //add reference
            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_bookings',function(Blueprint $table){
            $table->dropForeign('t_bookings_tour_id_foreign');
        });
        
        Schema::dropIfExists('t_bookings');
    }
}
