<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTBookingPassengerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_passengers', function (Blueprint $table) {
            //primary key
            $table->increments('id');
            $table->text('special_request');
            $table->timestamps();
            
            //foreign keys
            $table->unsignedInteger('booking_id');
            $table->unsignedInteger('passenger_id');
            
            //add reference
            $table->foreign('booking_id')->references('id')->on('t_bookings')->onDelete('cascade');           
            $table->foreign('passenger_id')->references('id')->on('passengers')->onDelete('cascade');           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_booking_passengers',function(Blueprint $table){
            $table->dropForeign('t_booking_passengers_booking_id_foreign');
        });
        Schema::table('t_booking_passengers',function(Blueprint $table){
            $table->dropForeign('t_booking_passengers_passenger_id_foreign');
        });
        Schema::dropIfExists('t_booking_passengers');
    }
}
