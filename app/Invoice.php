<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //create the relationship
    public function booking()
    {
        return $this->belongsTo('App\Booking');
    }
}
