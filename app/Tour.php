<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    //create relationships
    public function tour_dates(){
        return $this->hasMany('App\Tour_date');
    }
    public function bookings(){
        return $this->hasMany('App\Booking');
    }
}
