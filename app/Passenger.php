<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passenger extends Model
{
    //create relationships
    public function booking_passenger(){
        return $this->hasMany('App\Booking_passenger');
    }
}
