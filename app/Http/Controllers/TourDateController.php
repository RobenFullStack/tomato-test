<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tour_date;
use App\Tour;

use App\Http\Requests;
use App\Http\Resources\TourDate as TourDateResource;
use PHPUnit\Util\Json;

class TourDateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tour_id)
    {
        //fetch 'enable' tour date
        $tour_dates = Tour_date::where([['tour_id',$tour_id],['status',1]] )->orderBy('created_at','desc')->paginate(20);

        //return collection of tours as a resource
        return TourDateResource::collection($tour_dates);
    }


    //create single tour_date, use in edit tour page, when add new date
    public function createSingle(Request $request)
    {   
        //$request should have a tour_date obj only short of id
        $tour_date = new Tour_date;
        $tour_date->tour_id = $request->input('tour_id');
        $tour_date->date = $request->input('date');
        $tour_date->status = $request->input('status');
        $tour_date->save();
        //fetch new data set, return to vue.js

        //fetch a singe tour
        $tour_id = $request->input('tour_id');
        $tour = Tour::findOrFail($request->input('tour_id'));

        //fetch all tour_date for this tour
        $tour_dates = Tour_date::where('tour_id',$tour_id)->orderBy('id','desc')->paginate(10);
       

        return response()->json([
            "tour" => $tour,
            "tour_dates" => $tour_dates
        ], 200);
    }

    /**
     * Store a newly created resource in storage || update existing record.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //check http verb to determin wether update a record or create a new record
        $tour_date = $request->isMethod('put')? Tour_date::findOrFail($request->tour_date_id):new Tour_date;

        $tour_date->id = $request->input('tour_date_id');
        $tour_date->tour_id = $request->input('tour_id');
        $tour_date->date = $request->input('date');
        $tour_date->status = $request->input('status');

        if($tour_date->save()){
            return new TourDateResource($tour_date);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //fetch a singe tour
        $tour_date = Tour_date::findOrFail($id);

        //return this single tour as resource
        return new TourResource($tour_date);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fetch a singe tour
        $tour_date = Tour_date::findOrFail($id);

        //delete this single tour
        if($tour_date->delete()){
            return new TourResource($tour_date);
        }
    }
}
