<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tour;
use App\Booking;
use App\Passenger;
use App\Booking_passenger;
use App\Http\Requests;
use App\Http\Resources\Booking as BookingDateResource;
use PHPUnit\Util\Json;
use App\Tour_date;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        //fetch bookings
        $bookings = Booking::all();

        //loop collection add require info for display on list view
        $new_bookings=[];

        foreach ($bookings as $booking) {
            //col need 1. booking_id 2. tour_name 3. tour_date 4. number of passengers
            $new_booking=new Booking;
            $new_booking->id = $booking->id;
            $new_booking->tour_date = $booking->tour_date;
            //get tour_name
            $tour = Tour::findOrFail($booking->tour_id);
            $new_booking->tour_name = $tour->name;
            
            //get number of passengers
            $new_booking->number_of_passengers = Booking_passenger::where('booking_id',$booking->id)->count();
            
            array_push($new_bookings,$new_booking);
        }
        //return
        return response()->json([
            "bookings" => $new_bookings
        ], 200);        
        
    }

    /**
     * Create a new record
    */
    public function create(Request $request)
    {
        //create new booking obj
        $new_booking = new Booking;

        //fetch booking information from $request and map data
        $res_booking = $request->booking;

        $new_booking->tour_id = $res_booking["tour_id"];
    
        $new_booking->tour_date = $res_booking["tour_date"];
        $new_booking->status = 1;

        $new_booking->save();

        //get new_booking id for -> save passengers
        $booking_id = $new_booking->id;

        //fetch passenger information from $request and map data
        $res_passengers = $request->passengers;
        
        //loop all passengers save it to databse one by one
        foreach($res_passengers as $passenger)
        {
            $new_passenger = new Passenger;
            $new_passenger->given_name = $passenger["given_name"];
            $new_passenger->surname = $passenger["surname"];
            $new_passenger->email = $passenger["email"];
            $new_passenger->mobile = $passenger["mobile"];
            $new_passenger->passport = $passenger["passport"];
            $new_passenger->birth_date = $passenger["birth_date"];
            $new_passenger->status = 1;

            $new_passenger->save();

            //create record in booking_passenger table
            $new_booking_passenger = new Booking_passenger;
            $new_booking_passenger->booking_id = $booking_id;
            $new_booking_passenger->passenger_id = $new_passenger->id;
            $new_booking_passenger->special_request = $passenger["special_request"];

            $new_booking_passenger->save();
        }

        //return
        return response()->json([
            "booking" => $new_booking
        ], 200);
    }




    /**
     * update existing record.
     */
    public function store(Request $request)
    {
        //ToDo: now I delete all relationship booking<->passenger, then add again depending on new request
        //      this is not a good way, need find a better way to solve this problem, properly 'passport'



        //create new booking obj
        $new_booking = Booking::findOrFail($request->booking["id"]);

        //fetch booking information from $request and map data
        $res_booking = $request->booking;

        //update info
        $new_booking->tour_id=$res_booking["tour_id"];
        $new_booking->tour_date = $res_booking["tour_date"];
        $new_booking->status = $res_booking["status"];
        //save to database
        $new_booking->save();

        

        //get new_booking id for -> save passengers
        $booking_id = $new_booking->id;

        //remove all passenger belongs to this booking
        $booking_passengers = Booking_passenger::where('booking_id',$booking_id)->get();

        foreach($booking_passengers as $booking_passenger)
        {
            $passenger = Passenger::findOrFail($booking_passenger->passenger_id);
            $passenger->delete();
        }


        //fetch passenger information from $request and map data
        $res_passengers = $request->passengers;
        //loop all passengers save it to databse one by one
        foreach($res_passengers as $passenger)
        {
            $new_passenger = new Passenger;
            $new_passenger->given_name = $passenger["given_name"];
            $new_passenger->surname = $passenger["surname"];
            $new_passenger->email = $passenger["email"];
            $new_passenger->mobile = $passenger["mobile"];
            $new_passenger->passport = $passenger["passport"];
            $new_passenger->birth_date = $passenger["birth_date"];
            $new_passenger->status = 1;

            $new_passenger->save();

            //create record in booking_passenger table
            $new_booking_passenger = new Booking_passenger;
            $new_booking_passenger->booking_id = $booking_id;
            $new_booking_passenger->passenger_id = $new_passenger->id;
            $new_booking_passenger->special_request = $passenger["special_request"];

            $new_booking_passenger->save();
        }

        //return
        return response()->json([
            "booking" => $new_booking
        ], 200);

    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        //fetch a singe tour
        $booking = Booking::findOrFail($id);

        //fetch tour
        $tour = Tour::findOrFail($booking->tour_id);

        $booking->tour_name = $tour->name;

        //tour dates
        $tour_dates = Tour_date::where('tour_id',$tour->id)->where('status',1)->get();


        //fetch all passengers
        $Booking_passengers = Booking_passenger::where('booking_id',$booking->id)->get();//first() get one, get() get all
        $passengers=[];
        
        foreach($Booking_passengers as $Booking_passenger)
        {
            $passenger = Passenger::findOrFail($Booking_passenger->passenger_id);

            //fetch special_request
        
            $passenger->special_request = $Booking_passenger->special_request;
            array_push($passengers,$passenger);
        }

        return response()->json([
            "tour_dates"=>$tour_dates,
            "booking"=>$booking,
            "passengers"=>$passengers
        ],200);
    }

}
