<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Passenger;
use App\Booking_passenger;

class PassengerController extends Controller
{

    /**
     * Store a newly created resource in storage || update existing record.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //check http verb to determin wether update a record or create a new record
        $passenger = new Passenger;

        
        $booking_id = $request->input('booking_id');
        $passenger->given_name = $request->input('given_name');
        $passenger->status = $request->input('status');
        $passenger->surname = $request->input('email');
        $passenger->mobile = $request->input('mobile');
        $passenger->passport = $request->input('password');
        $passenger->birth_date=$request->input('birth_date');
        

        if($passenger->save()){
            $booking_passenger=new Booking_passenger;
            $booking_passenger->passenger_id=$passenger->id;
            $booking_passenger->booking_id=$booking_id;
            $booking_passenger->special_request = $request->input('sepcail_request');
            $booking_passenger->save();
            return new PassengerDateResource($passenger);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fetch a singe tour
        $passenger = Passenger::findOrFail($id);

        //delete this single tour
        if($passenger->delete()){
            return new PassengerResource($passenger);
        }
    }
}
