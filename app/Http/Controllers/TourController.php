<?php

namespace App\Http\Controllers;

use App\Tour;
use App\Tour_date;
use Illuminate\Http\Request;
use Psy\Util\Json;

//use App\Http\Resources\Tour as TourResource;

class TourController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //fetch tours
        $tours = Tour::where('status', 1)->orderBy('id', 'desc')->paginate(10);

        return response()->json([
            "tours" => $tours,
        ], 200);

    }

    //create Tour with tour_dates
    public function create(Request $request)
    {
        //1. create new object as tour 
        $new_tour = new Tour;
        $tour = $request->tour;
        $new_tour->name = $tour["name"];
        $new_tour->itinerary = $tour["itinerary"];
        $new_tour->status = $tour["status"];

        //2. save in database
        $new_tour->save();

        //3. fetch tour id
        $tour_id = $new_tour->id;

        //4. loop the Array of tour_dates
        $tour_dates = $request->tour_dates;

        foreach($tour_dates as $tour_date)
        {
            //4-1. add tour_id to each tour_date
            $new_tourDate = new Tour_date;
            $new_tourDate->tour_id = $tour_id;
            $new_tourDate->date = $tour_date["date"];
            $new_tourDate->status = 1;

            //4-2. save in database

            $new_tourDate->save();
        }

        $new_dates = Tour_date::where('tour_id',$tour_id)->paginate(5);

        //5. return
        return response()->json([
            "tour" => $new_tour,
            "tour_dates" => $new_dates,
        ], 200);
    }

    //update Tour with tour_dates
    public function store(Request $request)
    {
        // ToDo: handle update errors
        //fetch the original record of tour
        $tour = $request->tour;
        $old_tour = Tour::findOrFail($tour["id"]);
        $old_tour->name = $tour["name"];
        $old_tour->itinerary = $tour["itinerary"];
        $old_tour->status = $tour["status"];
        $old_tour->save();
        /**update tour-date */
        //1. fetch tour_date array from request
        $tour_dates = $request->tour_dates;
        //2. update all records

        foreach ($tour_dates as $tour_date) {
            $old_date = Tour_date::findOrFail($tour_date["id"]);
            //only status need change
            $old_date->status = $tour_date["status"];
            //save changes to database
            $old_date->save();
        }

        return response()->json([
            "tour" => $tour,
            "tour_dates" => $tour_dates,
        ], 200);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        //fetch a singe tour
        $tour = Tour::findOrFail($id);

        //fetch all tour_date for this tour
        $tour_dates = Tour_date::where('tour_id', $id)->orderBy('id', 'desc')->paginate(10);

        //return this single tour as resource
        return response()->json([
            "tour" => $tour,
            "tour_dates" => $tour_dates,
        ], 200);
    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
    // public function destroy($id)
    // {
    //     //fetch a singe tour
    //     $tour = Tour::findOrFail($id);

    //     //delete this single tour
    //     if ($tour->delete()) {
    //         return response()->json([
    //             "tour" => $tour,
    //         ], 200);
    //     }
    // }
    */
}
