<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking_passenger extends Model
{
    //create the relationship
    public function booking()
    {
        return $this->belongsTo('App\Booking');
    }

    public function passenger()
    {
        return $this->belongsTo('App\Passenger');
    }
}
