<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //create the relationship
    public function tour()
    {
        return $this->belongsTo('App\Tour');
    }

    public function invoice()
    {
        return $this->hasOne('App\Invoice');
    }
    
    public function booking_passenger(){
        return $this->hasMany('App\Booking_passenger');
    }
}
