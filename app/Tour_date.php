<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour_date extends Model
{
    //create the relationship
    public function tour()
    {
        return $this->belongsTo('App\Tour');
    }
}
