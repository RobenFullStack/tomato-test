<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route for Tour
    //list the tours
    Route::get('tours','TourController@index');

    //show a single tour
    Route::get('tour/{id}','TourController@show');

    //create new tour
    Route::post('tour','TourController@create');

    //update existing tour
    Route::put('tour','TourController@store');

//Route for Tour_date
    
    //create new tour date
    Route::post('newdate','TourDateController@createSingle');
    
    //update existing tour date
    Route::put('tourdate','TourDateController@store');

//Route for Booking
    //list bookings records
    Route::get('booking','BookingController@index');
    
    //show a single booking record
    Route::get('booking/{id}','BookingController@show');
    
    //create new booking record
    Route::post('booking','BookingController@create');
    
    //update existing booking record
    Route::put('booking','BookingController@store');

//Route for passenger
    //create new passenger record
    Route::post('passenger','PassengerController@create');
    
    //delete passenger
    Route::delete('passenger/{id}','PassengerController@destroy');
        