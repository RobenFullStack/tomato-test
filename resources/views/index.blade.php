<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">

        <script>window.Laravel={csrfToken:'{{csrf_token()}}'}</script>
        <title>Booking Tours</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    </head>
    <body>
        <div id="app">
            <div class="container">
                <nav>
                    <router-link to="/">Home</router-link>
                    <router-link to="/tour/edit">eidt tour</router-link>
                </nav>



                <router-view></router-view>

            </div>
            
        </div>

        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
