import CreateTour from "./components/create-tour.vue";
import EditTour from "./components/edit-tours.vue";
import Tours from "./components/tours";
import CreateBooking from "./components/create-booking.vue";
import ListBooking from "./components/list-booking.vue";
import EditBooking from "./components/edit-booking.vue";

export const routes = [
    {
        path: "/",
        name: "tours",
        component: Tours
    },
    {
        path: "/tour/create",
        name: "create-tour",
        component: CreateTour
    },
    {
        path: "/tour/edit/:id",
        name: "edit-tour",
        component: EditTour
    },
    {
        path: "/booking/create/:id",
        name: "create-booking",
        component: CreateBooking
    },
    {
        path: "/booking/list",
        name:"list-bookings",
        component: ListBooking
    },
    {
        path:"/booking/edit/:id",
        name:"edit-booking",
        component:EditBooking
    }
];
