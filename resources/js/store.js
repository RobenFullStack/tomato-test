export default {//need a white space bewteen '{' and 'default'
    state:{
        tours:[],
        bookings:[]
    },
    getters: {
        tours(state){
            return state.tours;
        },
        bookings(state){
            return state.bookings;
        }
    },
    mutations: {
        updateTours(state,payload){
            state.tours =payload.data;
        },
        updateBookings(state,payload){
            state.bookings =payload;
            console.log(payload);
        }
    },
    actions: {
        getTours(context){
            axios.get('/api/tours').then(res=>{
                context.commit('updateTours',res.data.tours);
            });
        },
        getBookings(context){
            axios.get('/api/booking').then(res=>{
                console.log(res);
                context.commit('updateBookings',res.data.bookings);
            })
        }
    }
}

/* basic in store we should have those attributes
state:{},
mutations: {},
getters: {},
actions: {}
 */