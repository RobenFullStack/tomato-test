require("./bootstrap");
import Vue from "vue";
import VueRouter from "vue-router";
import Vuex from "vuex";
//import routes infos from external file,remeber add to 'const app'
import { routes } from "./routes.js";
//import store
import StoreData from "./store.js";

//main component 'App.vue'
import App from "./components/App.vue";

Vue.use(VueRouter);
Vue.use(Vuex);

//after this declaration all component can call store by 'this.$store'
const store = new Vuex.Store(StoreData);

const router = new VueRouter({
    routes,
    mode: "history"
});

const app = new Vue({
    el: "#app",
    router, //need add here otherwise routes will not be knowed by other components
    store,
    components: { App }
});
